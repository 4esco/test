{if $category_data.show_title == "Y" and $location_data.dispatch == "categories.view" and $category_data.category_title != ""}
    {$category_data.category_title} 
{else}
    {if $page_title}
        {$page_title}
    {else}
        {foreach from=$breadcrumbs item=i name="bkt"}
            {if !$smarty.foreach.bkt.first}{$i.title|strip_tags}{if !$smarty.foreach.bkt.last} :: {/if}{/if}
        {/foreach}
        {if !$skip_page_title && $location_data.title}{if $breadcrumbs|count > 1} - {/if}{$location_data.title}{/if}
    {/if}
{/if}