{if $order_info.tax_number}
<tr class="ty-orders-summary__row">
    <td>{__("tax_number")}:</td>
    <td style="width: 57%">
        {$order_info.tax_number}
    </td>
</tr>
{/if}
{if $order_info.gift_box_message}
<tr class="ty-orders-summary__row">
    <td>{__("gift_box_message")}:</td>
    <td style="width: 57%">
        {$order_info.gift_box_message}
    </td>
</tr>
{/if}