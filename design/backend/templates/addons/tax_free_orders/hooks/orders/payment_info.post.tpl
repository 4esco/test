{if $order_info.tax_number}
    <div class="control-group">
        <div class="control-label">{__("tax_number")}</div>
        <div id="tygh_payment_info" class="controls">{$order_info.tax_number}</div>
    </div>
{/if}
{if $order_info.gift_box_message}
    <div class="control-group">
        <div class="control-label">{__("gift_box_message")}</div>
        <div id="tygh_payment_info" class="controls">{$order_info.gift_box_message}</div>
    </div>
{/if}