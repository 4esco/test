<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_order_id_prefix_place_order(&$order_id, $action, $order_status, $cart, $auth) {
    
    if (Registry::get('addons.order_id_prefix.order_prefix') == 'prefix_h') {
        
        $prefix = 'H'.$order_id;
        
    }
    else {
        
        $prefix = 'N'.$order_id;
        
    }
    
    db_query('UPDATE ?:orders SET order_number_pref = ?s WHERE order_id = ?i', $prefix, $order_id);

}