<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    
    if ($mode == 'update_steps') {
                    
        if ($_REQUEST['update_step'] == 'step_two' || $_REQUEST['update_step'] == 'step_one' && !$errors) {

            $_SESSION['tax_number'] = $_REQUEST['tax_number'];
    
        }
        
        if (!empty($_REQUEST['next_step']) && ($_REQUEST['next_step'] == 'step_three' || $_REQUEST['next_step'] == 'step_four')) {
            
            $_SESSION['gift_box_message'] = $_REQUEST['gift_box_message'];
            
        }
    }
}
