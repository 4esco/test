<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_tax_free_orders_place_order(&$order_id, $action, $order_status, $cart, $auth) {

    if ($order_id) {
             
        db_query('UPDATE ?:orders SET tax_number = ?s, gift_box_message = ?s WHERE order_id = ?i', $_SESSION['tax_number'], $_SESSION['gift_box_message'], $order_id);
        unset($_SESSION['tax_number']);
        unset($_SESSION['gift_box_message']);
        
    }

}