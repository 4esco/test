<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_product_thumbnails_get_order_items_info_post(&$order, $v, $k) {
    
    fn_gather_additional_products_data($order['products'], array('get_icon' => true, 'get_detailed' => true));

}