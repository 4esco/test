<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    return;
}

if ($mode == 'update') {
    
    Registry::set('navigation.tabs.additional_cat_img', array (
        'title' => __('additional_cat_img'),
        'js' => true
    ));

}