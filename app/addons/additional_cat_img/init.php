<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'get_category_data_post',
    'update_category_post',
    'get_categories_post'
);