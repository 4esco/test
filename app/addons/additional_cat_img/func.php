<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_additional_cat_img_get_category_data_post(&$category_id, $field_list, $get_main_pair, $skip_company_condition, $lang_code, &$category_data) {
    
    $category_data['image_pairs'] = fn_get_image_pairs($category_id, 'category', 'A', true, true, $lang_code);

}

function fn_additional_cat_img_update_category_post($category_data, $category_id, $lang_code) {
    
    fn_attach_image_pairs('category_additional', 'category', $category_id, $lang_code);
    fn_attach_image_pairs('category_add_additional', 'category', $category_id, $lang_code);
    
}

function fn_additional_cat_img_get_categories_post(&$categories_list, $params, $lang_code) {
    foreach ($categories_list as $k => $v) {
        $categories_list[$k]['image_pairs'] = fn_get_image_pairs($v['category_id'], 'category', 'A', true, true, $lang_code);
    }
}